
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System;

namespace ProjetoNUnit
{
    [TestFixture]
    public class TesteNavegador2
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void SelecionarComboBox()
        {
             /*Montagem de ambiente*/
            int index = 0;
            string url = "http://the-internet.herokuapp.com/dropdown";
            IWebDriver driver = new ChromeDriver();
            
            /*Execução*/
            driver.Navigate().GoToUrl(url);
            SelectElement combobox = new SelectElement(driver.FindElement(By.Id("dropdown")));
            combobox.SelectByIndex(index);
            
            /*Validação*/
            
            //Assert.IsTrue(combobox.Equals(0));
            //Assert.IsFalse(carro.Equals("Palio"));
            //Assert.AreEqual();
        }
        [Test]
        public void AcessarCasosDeTests()
        {
            /*Montagem de Ambiente*/
            string url = "http://blackmirror.crowdtest.me.s3-website-us-east-1.amazonaws.com/auth";        
            IWebDriver driver = new ChromeDriver();    

            /*Execução*/     
            driver.Navigate().GoToUrl(url);
            driver.FindElement(By.Id("login")).SendKeys("Seu email");
            driver.FindElement(By.Id("password")).SendKeys("Sua senha");            
            driver.FindElement(By.XPath("//*[@class='btn btn-crowdtest btn-block']")).Click();          
            
            //Espera até que o próximo elemento esteja visivel
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='btn btn-crowdtest mr-1']")));

            driver.FindElement(By.XPath("//*[@class='btn btn-crowdtest mr-1']")).Click();
            
            //Navegação pela Tela
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='ng-input']")));
            driver.FindElement(By.XPath("//*[@class='ng-input']")).Click();            
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='ng-option-label']")));
            driver.FindElement(By.XPath("//*[@class='ng-option-label']")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='text-line']")));
            driver.FindElement(By.XPath("//*[@class='top-line']")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@class='mat-tab-label-content']")));           
            driver.FindElement(By.XPath("//*[starts-with(@class,'mat-tab-label')]")).Click();


            /*Validação*/           
            
            

        }
       
              

    }
}