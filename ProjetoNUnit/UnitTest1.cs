using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ProjetoNUnit
{
    [TestFixture]
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            string carro = "Gol";
            Assert.IsTrue(carro.Equals("Gol"));
            Assert.IsFalse(carro.Equals("Palio"));
            Assert.AreEqual("Gol", carro);
        }

         [Test]
        public void TesteTelaGoogle()
        {
            string url = "https://www.google.com/";
            IWebDriver driver = new ChromeDriver();
            
            driver.Navigate().GoToUrl(url);
            string titulo = driver.Title;
            driver.FindElement(By.Name(titulo));

            Assert.IsTrue(url.Equals("https://www.google.com/"));            
            Assert.AreEqual("https://www.google.com/", url);
        }      

    }
}