using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using System;


namespace ProjetoNUnit
{
    [TestFixture]
    public class TesteNavegador
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TesteChrome()
        {
            /*Montagem do ambiente*/
            string url = "https://www.google.com/";
            IWebDriver driver =  new ChromeDriver();

            /*Execução*/
            driver.Navigate().GoToUrl(url);
            driver.FindElement(By.Name("q"));
            driver.FindElement(By.Name("q")).SendKeys("Base2");
            //driver.FindElement(By.Name("btnK")).Click();
            driver.FindElement(By.Name("q")).SendKeys(Keys.Enter);

            /*Validação*/

            Assert.IsTrue(url.Equals("https://www.google.com/"));
        }
        [Test]
        public void FillingTest()
        {
            /*Montagem do ambiente*/
            string mensagemSucesso;
            string url = "https://ultimateqa.com/filling-out-forms/";
            IWebDriver driver =  new ChromeDriver();

            /*Execução*/
            driver.Navigate().GoToUrl(url);
            driver.FindElement(By.Id("et_pb_contact_name_0")).SendKeys("Anderson");
            driver.FindElement(By.Id("et_pb_contact_message_0")).SendKeys("Treinamento base2");
            
            
            //timer de 10 segundos, pois estava executando muito rápido e apresentando erro.
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);            

            //Acão de clicar no botão
            driver.FindElement(By.Name("et_builder_submit_button")).Click();
            
            mensagemSucesso = driver.FindElement(By.XPath("//*[@id='et_pb_contact_form_0']/div/p")).Text;
            
            /*Validação*/

            Assert.AreEqual("Form filled out successfully", mensagemSucesso);
        }
        [Test]
        public void SegundoFillingTest()
        {
            /*Montagem do ambiente*/
            string mensagemSucesso;
            string url = "https://ultimateqa.com/filling-out-forms/";
            IWebDriver driver =  new ChromeDriver();

            /*Execução*/
            driver.Navigate().GoToUrl(url);
            driver.FindElement(By.Id("et_pb_contact_name_0")).SendKeys("Anderson");        
                        
            //timer de 10 segundos, pois estava executando muito rápido e apresentando erro.
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Name("et_builder_submit_button")));
            
            driver.FindElement(By.Name("et_builder_submit_button")).Click();                        
            
            mensagemSucesso = driver.FindElement(By.XPath("//*[@id='et_pb_contact_form_0']/div/p")).Text;
            
            /*Validação*/

            Assert.AreEqual("Please, fill in the following fields:", mensagemSucesso);
            Assert.IsTrue(mensagemSucesso.Equals("Please, fill in the following fields:"));
            Assert.IsFalse(mensagemSucesso.Equals("Please, fill in the following fields:"));
        }
        [Test]
        public void LoginUdemyTest()
        {
            /*Montagem de ambiente*/
            string mensagem;
            string url = "https://www.udemy.com/join/login-popup/?locale=pt_BR&response_type=html&next=https%3A%2F%2Fwww.udemy.com%2Fcourse%2Fcriando-testes-online-com-google-formularios%2F%3Futm_source%3Dadwords%26utm_medium%3Dudemyads%26utm_campaign%3DWebindex_Catchall_la.PT_cc.BR%26utm_term%3D_._ag_114148736799_._ad_485704569542_._de_c_._dm__._pl__._ti_dsa-43377404311_._li_1001566_._pd__._%26gclid%3DCj0KCQiA9P__BRC0ARIsAEZ6irjoCHI8tWu60puZ2txxkjOtT9UKmvG2UIkvumDafKPeqKfybbCV6F4aAm98EALw_wcB";
            IWebDriver driver = new ChromeDriver(); 

            /*Execução*/
            driver.Navigate().GoToUrl(url); 
            driver.FindElement(By.Name("email")).SendKeys("sistemas.anderson01@gmail.com");
            driver.FindElement(By.Name("password")).SendKeys("1234567");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.FindElement(By.Name("submit")).Click();

            mensagem = driver.FindElement(By.XPath("//*[@id='login-form']/div[1]/div/div")).Text;
            /*Validação*/

             Assert.AreEqual("Ocorreu um problema ao fazer login. Verifique seu e-mail e senha ou crie uma conta.:", mensagem);
        }

    }
}